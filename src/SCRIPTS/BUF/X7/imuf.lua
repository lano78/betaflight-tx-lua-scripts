
return {
    read              = 227, -- MSP_IMUF_CONFIG
    write             = 228, -- MSP_SET_IMUF_CONFIG
    eepromWrite       = true,
    reboot            = false,
    title             = "Imuf",
    minBytes          = 16,
    text= {
      { t = "Q", x = 35, y = 14, to = SMLSIZE },
      { t = "LPF", x = 65, y = 14, to = SMLSIZE },
      { t = "IMUF", x = 90, y = 14, to = SMLSIZE },
      { t = "ROLL", x = 5, y = 24, to = SMLSIZE },
      { t = "PITCH", x = 5, y = 34, to = SMLSIZE },
      { t = "YAW", x = 5, y = 44, to = SMLSIZE },
      { t = "mode", x = 90, y = 24, to = SMLSIZE },
	    { t = "imuf_w", x = 90, y = 34, to = SMLSIZE },
    },
    fields = {
      { x = 35,  y = 24, min = 0, max = 16000, to = SMLSIZE, vals = { 3, 4 } },
	  { x = 35,  y = 34, min = 0, max = 16000, to = SMLSIZE, vals = { 5, 6 } },
	  { x = 35,  y = 44, min = 0, max = 16000, to = SMLSIZE, vals = { 7, 8 } },
	  { x = 65,  y = 24, min = 0, max = 450,   to = SMLSIZE, vals = { 11, 12 } },
      { x = 65,  y = 34, min = 0, max = 450,   to = SMLSIZE, vals = { 13, 14 } },
      { x = 65,  y = 44, min = 0, max = 450,   to = SMLSIZE, vals = { 15, 16 } },
      { x = 125, y = 24, min = 0, max = 255,   to = SMLSIZE, vals = { 1, 2 } },
      { x = 125, y = 34, min = 0, max = 300,   to = SMLSIZE, vals = { 9, 10 } },
    },
}
